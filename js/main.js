$(function(){

	/*---------- SETUP ----------*/

		var jRes = jRespond([
			{
				label: 'small',
				enter: 0,
				exit: 750
			},{
				label: 'large',
				enter: 751,
				exit: 10000
			}
		]);

		jRes.addFunc({
			breakpoint: '*',
			enter: function() {
				var breakpoint = jRes.getBreakpoint();
					$('body').addClass(breakpoint);
			},
			exit: function() {
					$("body").removeAttr('class');
			}
		});

		var id = getUrlParameters("id", "", true);
		returnMessage(id);



	/*---------- TRIGGERS ----------*/

		$('.burger').on('click', showMenu);
		$('.links li').on('click', goToSection);
		$('.map').on('click', showFlightInfo);
		$('.submit').on('click', checkForm);



	/*---------- FUNCTIONS ----------*/

		function checkForm(){

			var nameVal = $('#name').val();
			var emailVal = $('#email').val();
			var answer = $('input:radio[name=answer]');

			if(nameVal === ''){
				alert('Please enter your name');
				return false;
			}else if(emailVal === ''){
				alert('Please enter your email address');
				return false;
			}
			else if(!answer.is(':checked')){
				alert('Please select either \'Yes\' or \'No\'');
				return false;
			}else{
				return true;
			}

			
		}

		function goToSection(){
			var link = $(this).attr('data-link');
			$('.menu_small ul').slideUp();
			$("html, body").animate({ scrollTop: $('.'+link).offset().top }, 250);
		}

		function showFlightInfo(){
			var country = $(this).attr('data-country');
			$('.map').removeClass('active');
			$(this).addClass('active');
			$('.flight_info').hide();
			$('.flight_info.'+country).show();
		}

		function showMenu(){
			$('.menu_small ul').slideToggle();
		}

		function returnMessage(id){
			// 1 yes, 2 no, 3 error
			if(id === '1'){
				$('.form').hide();
				$('.message').html('Thanks for letting us know. We can\'t wait to see you in Barbados!').show();
				$("html, body").animate({ scrollTop: $('.rsvp').offset().top }, 1000);
			}else if(id === '2'){
				$('.form').hide();
				$('.message').html('Thanks for letting us know. It\'s a shame you can\'t come, we\'ll miss you!').show();
				$("html, body").animate({ scrollTop: $('.rsvp').offset().top }, 1000);
			}else if(id === '3'){
				$('.message').html('Sorry, it seems like the form didn\'t send. Try sending it off again, or alternatively <a class="gold" href="mailto:wedding@johnandstef.com">drop us an email</a>.').show();
				$("html, body").animate({ scrollTop: $('.rsvp').offset().top }, 1000);
			}
		}

		function getUrlParameters(parameter, staticURL, decode){
			var currLocation = (staticURL.length)? staticURL : window.location.search;
			if(currLocation !== ''){
				var parArr = currLocation.split("?")[1].split("&");
				var returnBool = true;

				for(var i = 0; i < parArr.length; i++){
					parr = parArr[i].split("=");
					if(parr[0] == parameter){
						return (decode) ? decodeURIComponent(parr[1]) : parr[1];
						returnBool = true;
					}else{
						returnBool = false;
					}
				}

				if(!returnBool) return false;
			}
		}



});
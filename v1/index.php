
<!DOCTYPE html>
<html>
<head>
  	<title>John &amp; Stef are getting married</title>
	<meta name="viewport" content="width=device-width, maximum-scale=1.0" />
	<meta name="description" content="The website to celebrate the marriage of John Hudson and Stephanie Marshall">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">

	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script id="javascript" type="text/javascript" src="js/jrespond.js"></script>
	<script id="javascript" type="text/javascript" src="js/main.js"></script>
</head>
<body>

	<div class="menu_small">

		<div class="menu_bar cf">
			<div class="left logo">
				J <span class="gold">&amp;</span> S
			</div>

			<div class="right burger">
				<div class="bar bar_1"></div>
				<div class="bar bar_2"></div>
				<div class="bar bar_3"></div>
			</div>
		</div>

		<ul class="links">
			<li data-link="ceremony">The Ceremony</li>
			<li data-link="barbados">Barbados</li>
			<li data-link="rsvp">RSVP</li>
		</ul>

	</div>

	<div class="menu_large">

		<div class="menu_content">

			<div class="logo">
				J <span class="gold">&amp;</span> S
			</div>

			<ul class="links">
				<li data-link="ceremony">The Ceremony</li>
				<li data-link="barbados">Barbados</li>
				<li data-link="contact">Contact</li>
				<!-- <li data-link="rsvp">RSVP</li> -->
			</ul>

		</div>

	</div>


	<div class="ceremony outer">
		<div class="middle">
			<div class="inner">
				<img class='invite' src='img/save_the_date_logo.png' width="100%" />
			</div>
		</div>
	</div>

	<div class="sections">

		<div class="section barbados cf">
			<div class="section_content right">

				<h1>Barbados</h1>

				<br />

				<h2>Flights</h2>

				<p>There's only one airport on the island - Barbados Grantley Adams International Airport (BGI), so select where you're coming from for a few options of how to get you there. </p>

				<div class="locations cf">					

					<div class="country">
						<div class="map uk" data-country="uk"></div>
						UK
					</div>
					
					<div class="country">
						<div class="map north_america" data-country="north_america"></div>
						North America
					</div>

				</div>

				<div class="flight_info uk">
					<p>Both Virgin Atlantic and British Airways fly direct to Barbados from London, Gatwick every day. Virgin Atlantic also flies direct from Manchester Airport, but flights are not as frequent. Tickets can cost between &pound;500 and &pound;600 return.

					<br /><br />The best time to buy flights is either January (in the sales) or May/June when specials will come out again for the end of the summer. We'll be keeping an eye on prices and will let you know if there are any super deals.</p>
				</div>

				<div class="flight_info north_america">
					<p>Several airlines serve North America from Barbados, they include:</p>
					<ul>
						<li>American Airlines flies direct from Miami to BGI twice per day. </li>
						<li>Jet Blue flies direct from JFK, in New York to BGI once a day.</li>
						<li>US Airways flies direct from Charlotte, North Carolina once per week.</li>
						<li>Air Canada and West Jet fly direct from Toronto to BGI every day.</li>
						<li>Air Canada flies direct from Montreal to BGI twice a week.</li>
					</ul><br />
					<p>Of course there are infinite connecting options all across the States and Canada, so there are many deals to be found!</p>
				</div>

				<h2>Accommodation</h2>

				<p>There are hundreds of hotels, villas, and condos in Barbados to choose from. There is so much choice that it can get overwhelming, so we put together a few tips to help guide you in the right direction.</p>

				<ul>
					<li>
						You should aim to be as close to the beach as possible; there are some good value options that are beachfront and we think it is definitely worth it!
					</li>
					<li>
						We would always recommend somewhere with self catering as eating out in Barbados can get a bit pricey. 
					</li>
					<li>
						In terms of location, you will get the best value for money on the South Coast. That’s in the parishes of St. Michael and Christ Church. In comparison to the West Coast, where the rich and famous stay, the South Coast is less pretentious. It’s also more suited to pedestrians, with plenty of bars, restaurants and supermarkets in walking distance. Hastings and Worthing are particularly convenient areas to stay.
					</li>
					<li>
						When looking at Tripadvisor reviews, just keep in mind that Barbados is a top destination for the rich and famous. Some people in this category have, let’s say particular taste, and can be highly critical of details. This means that some places that I would consider <i>pure luxury</i> have less than stellar Tripadvisor ratings. I’m not suggesting you should ignore negative reviews, just please take them with a pinch of salt and consider the details that are being criticised. 
					</li>
				</ul>


			</div>
		</div>

		<div class="section contact cf">
			<div class="section_content right">

				<h1>Contact</h1>

				<br /><br /><br />

				<p>If you need any more information from us or have any questions drop us an email at <a href="mailto:wedding@johnandstef.com">wedding@johnandstef.com</a></p>

			</div>
		</div>

		<!-- <div class="section rsvp cf">
			<div class="section_content right">

				<h1>RSVP</h1>

			</div>
		</div> -->

	</div>
  
</body>


</html>


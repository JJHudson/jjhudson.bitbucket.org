$(function(){

	/*---------- SETUP ----------*/

		var jRes = jRespond([
			{
				label: 'small',
				enter: 0,
				exit: 750
			},{
				label: 'large',
				enter: 751,
				exit: 10000
			}
		]);

		jRes.addFunc({
			breakpoint: '*',
			enter: function() {
				var breakpoint = jRes.getBreakpoint();
					$('body').addClass(breakpoint);
			},
			exit: function() {
					$("body").removeAttr('class');
			}
		});



	/*---------- TRIGGERS ----------*/

		$('.burger').on('click', showMenu);
		$('.links li').on('click', goToSection)
		$('.map').on('click', showFlightInfo);



	/*---------- FUNCTIONS ----------*/

		function goToSection(){
			var link = $(this).attr('data-link');
			$('.menu_small ul').slideUp();
			$("html, body").animate({ scrollTop: $('.'+link).offset().top }, 250);
		}

		function showFlightInfo(){
			var country = $(this).attr('data-country');
			$('.map').removeClass('active');
			$(this).addClass('active');
			$('.flight_info').hide();
			$('.flight_info.'+country).show();
		}

		function showMenu(){
			$('.menu_small ul').slideToggle();
		}



});